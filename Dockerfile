FROM python:3.10-alpine3.16

COPY requirements.txt /temp/requirements.txt
RUN apk add postgresql-client build-base postgresql-dev

RUN pip install -r /temp/requirements.txt

RUN adduser --disabled-password service-user
WORKDIR /store_bot
COPY . .
EXPOSE 8000


USER service-user
