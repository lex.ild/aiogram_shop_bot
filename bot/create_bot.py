import os

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

# Initialize bot and dispatcher
bot = Bot(token=os.environ.get('TOKEN'))

storage = MemoryStorage()

dp: Dispatcher = Dispatcher(bot, storage=storage)
