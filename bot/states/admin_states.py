from aiogram.dispatcher.filters.state import StatesGroup, State


class SendMailingState(StatesGroup):
    start_processing = State()
    get_text = State()