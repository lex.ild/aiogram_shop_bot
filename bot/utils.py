import os

import psycopg2
from openpyxl import Workbook
from openpyxl.reader.excel import load_workbook


def connect_to_db():
    # Параметры подключения к базе данных
    dbname = os.environ.get('POSTGRES_DB')
    user = os.environ.get('POSTGRES_USER')
    password = os.environ.get('POSTGRES_PASSWORD')
    host = os.environ.get('DB_HOST')
    port = os.environ.get('DB_PORT')

    # Подключение к базе данных
    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
    cursor = conn.cursor()

    return conn, cursor


def get_categories():
    try:
        conn, cursor = connect_to_db()

        cursor.execute("SELECT * FROM bot_admin_app_category")
        rows = cursor.fetchall()

        categories = []
        for row in rows:
            category = {
                'id': row[0],
                'name': row[1],
            }
            categories.append(category)

        cursor.close()
        conn.close()

        return categories

    except (Exception, psycopg2.Error) as error:
        print(f"Error while fetching categories from database: {error}")
        return None


def get_subcategories(category_id):
    try:
        conn, cursor = connect_to_db()

        query = "SELECT * FROM bot_admin_app_subcategory WHERE category_id = %s;"
        cursor.execute(query, (category_id,))
        rows = cursor.fetchall()

        subcategories = []
        for row in rows:
            subcategory = {
                'id': row[0],
                'name': row[1],
            }
            subcategories.append(subcategory)

        cursor.close()
        conn.close()

        return subcategories

    except (Exception, psycopg2.Error) as error:
        print(f"Error while fetching categories from database: {error}")
        return None


def get_products(subcategory_id):
    try:
        conn, cursor = connect_to_db()

        products = []

        query = "SELECT * FROM bot_admin_app_product WHERE subcategory_id = %s;"
        cursor.execute(query, (subcategory_id,))
        rows = cursor.fetchall()

        for row in rows:
            product = {
                'id': row[0],
                'name': row[1],
                'photo': row[2],
                'description': row[3],
                'price': row[6]
            }
            products.append(product)

        cursor.close()
        conn.close()

        return products

    except (Exception, psycopg2.Error) as error:
        print(f"Error while fetching categories from database: {error}")
        return None


def add_to_cart(user_id, product_id):
    try:
        # Подключение к базе данных
        conn, cursor = connect_to_db()

        # Проверка, существует ли уже запись с таким user_id и product_id
        cursor.execute("SELECT id, quantity FROM bot_admin_app_cart WHERE user_id = %s AND product_id = %s",
                       (str(user_id), product_id))
        row = cursor.fetchone()

        if row:
            # Если запись уже существует, то увеличиваем quantity на 1
            cart_id = row[0]
            quantity = row[1] + 1
            cursor.execute("UPDATE bot_admin_app_cart SET quantity = %s WHERE id = %s", (quantity, cart_id))
        else:
            quantity = 1
            # Если запись не существует, то создаем новую запись со значением quantity = 1
            cursor.execute("INSERT INTO bot_admin_app_cart (user_id, product_id, quantity) VALUES (%s, %s, %s)",
                           (str(user_id), product_id, quantity))

        # Применение изменений
        conn.commit()

        # Закрытие соединения
        cursor.close()
        conn.close()

        return quantity
    except Exception as e:
        # Обработка ошибок
        print("Ошибка при добавлении продукта в корзину:", e)
        return "Ошибка при добавлении продукта в корзину"


def get_cart_by_user(user_id):
    try:
        # Подключение к базе данных
        conn, cursor = connect_to_db()

        # Получение корзины пользователя с агрегированными результатами по user_id
        cursor.execute("""
            SELECT 
                c.user_id,
                c.quantity,
                p.photo,
                p.name AS product_name, 
                p.price,
                p.description,
                c.id,
                array_agg(c.product_id) AS product_ids 
            FROM 
                bot_admin_app_cart c 
            INNER JOIN 
                bot_admin_app_product p ON c.product_id = p.id 
            WHERE 
                c.user_id = %s
            GROUP BY 
                c.user_id,
                c.quantity,
                p.photo, 
                p.name,
                p.price,
                p.description,
                c.id
                
        """, (str(user_id),))
        cart = cursor.fetchall()

        cart_products = []
        for row in cart:
            product = {
                'id': row[0],
                'quantity': row[1],
                'photo': row[2],
                'name': row[3],
                'price': row[4],
                'description': row[5],
                'cart_id': row[6]
            }
            cart_products.append(product)

        # Закрытие соединения
        cursor.close()
        conn.close()

        return cart_products

    except Exception as e:
        # Обработка ошибок
        print("Ошибка при получении корзины пользователя:", e)
        return None


def change_cart_quantity(cart_id, action):
    try:
        # Подключение к базе данных
        conn, cursor = connect_to_db()

        # Проверка, существует ли уже запись с таким user_id и product_id
        cursor.execute("SELECT id, quantity FROM bot_admin_app_cart WHERE id = %s",
                       (str(cart_id),))
        row = cursor.fetchone()
        if row:

            quantity = row[1]

            if action == 'increase':
                quantity += 1
            elif action == 'decrease':
                quantity -= 1

            if action == 'del' or quantity == 0:
                cursor.execute("DELETE FROM bot_admin_app_cart WHERE id = %s", (cart_id,))
                # Применение изменений
                conn.commit()

                # Закрытие соединения
                cursor.close()
                conn.close()
                return None
            else:
                cursor.execute("UPDATE bot_admin_app_cart SET quantity = %s WHERE id = %s", (quantity, cart_id))

            # Применение изменений
            conn.commit()

            # Закрытие соединения
            cursor.close()
            conn.close()

            return quantity

    except Exception as e:
        # Обработка ошибок
        print("Ошибка при изменении количества товара:", e)


def delete_all_cart_items(user_id):
    try:
        # Подключение к базе данных
        conn, cursor = connect_to_db()

        # Удаление всех товаров из корзины пользователя
        cursor.execute("DELETE FROM bot_admin_app_cart WHERE user_id = %s", (str(user_id),))
        conn.commit()

        # Закрытие соединения
        cursor.close()
        conn.close()

        print("Все товары из корзины пользователя удалены.")
    except Exception as e:
        print("Ошибка при удалении всех товаров из корзины пользователя:", e)


def save_subscriber(chat_id, username, phone, address):
    try:
        conn, cursor = connect_to_db()
        cursor.execute(
            """
            INSERT INTO bot_admin_app_subscriber (chat_id, username, phone, address) 
            VALUES (%s, %s, %s, %s)
            ON CONFLICT (chat_id) DO UPDATE 
            SET username = excluded.username, 
                phone = excluded.phone, 
                address = excluded.address
            """,
            (chat_id, username, phone, address)
        )
        conn.commit()
    except psycopg2.Error as e:
        print(f"Error saving subscriber: {e}")
        conn.rollback()
    finally:
        conn.close()


def get_subscriber(chat_id):
    try:
        conn, cursor = connect_to_db()
        cursor.execute(
                "SELECT * from bot_admin_app_subscriber WHERE chat_id = %s",
                (str(chat_id),)
            )
        row = cursor.fetchone()

        username = row[2]
        phone = row[3]
        address = row[4]

        return username, phone, address

    except psycopg2.Error as e:
        print(f"Error saving subscriber: {e}")
    finally:
        conn.close()


def get_subscribers():
    try:
        conn, cursor = connect_to_db()
        cursor.execute("SELECT * from bot_admin_app_subscriber")
        rows = cursor.fetchall()

        subscribers = []

        for row in rows:
            subscriber = {
                'subscriber_id': row[0],
                'user_id': row[1],
                'username': row[2],
                'phone': row[3],
                'address': row[4],
            }
            subscribers.append(subscriber)

        return subscribers

    except psycopg2.Error as e:
        print(f"Error saving subscriber: {e}")
    finally:
        conn.close()


def search_faq(query_text):
    # Подключение к базе данных
    conn, cursor = connect_to_db()

    # Выполнение SQL-запроса для поиска вопросов и ответов
    cursor.execute("SELECT id, question, answer FROM bot_admin_app_faq WHERE question LIKE %s",
                   ('%' + query_text + '%',))
    rows = cursor.fetchall()

    # Формирование результата в виде списка словарей
    results = [{'id': str(row[0]), 'question': row[1], 'answer': row[2]} for row in rows]

    # Закрытие соединения с базой данных
    cursor.close()
    conn.close()

    return results


# Функция для получения списка вопросов из базы данных
def get_faq_questions():
    # Подключение к базе данных
    conn, cursor = connect_to_db()

    # Выполнение SQL-запроса для получения списка вопросов
    cursor.execute("SELECT id, question FROM bot_admin_app_faq")
    rows = cursor.fetchall()

    cursor.close()
    conn.close()
    return rows


def get_faq_answer(question_id):
    # Подключение к базе данных
    conn, cursor = connect_to_db()

    # Выполнение SQL-запроса для получения ответа на вопрос
    cursor.execute("SELECT answer FROM bot_admin_app_faq WHERE id = %s", (question_id,))
    row = cursor.fetchone()

    # Закрытие соединения с базой данных
    cursor.close()
    conn.close()

    if row:
        return row[0]
    else:
        return "Ответ на данный вопрос не найден"  # Возвращаем сообщение, если ответ не найден


def save_order_to_excel(user_id, total_amount):
    # Создаем новый файл Excel и получаем активный лист

    user_data = get_subscriber(user_id)

    file_path = os.getcwd()
    try:
        workbook = load_workbook(file_path + '/orders.xlsx')
    except FileNotFoundError:
        # Если файл не найден, создаем новый
        workbook = Workbook()

    # Получаем активный лист
    sheet = workbook.active

    if sheet['A1'].value is None:
        sheet['A1'] = 'Имя пользователя'
        sheet['B1'] = 'Контактный телефон'
        sheet['C1'] = 'Адрес'
        sheet['D1'] = 'Сумма заказа'

    # Находим первую свободную строку
    row_num = sheet.max_row + 1

    # Записываем данные заказа в свободную строку
    sheet[f'A{row_num}'] = user_data[0]
    sheet[f'B{row_num}'] = user_data[1]
    sheet[f'C{row_num}'] = user_data[2]
    sheet[f'D{row_num}'] = total_amount

    # Сохраняем файл
    workbook.save(file_path + '/orders.xlsx')

    print(f'Данные заказа успешно сохранены в файл: {file_path}')
