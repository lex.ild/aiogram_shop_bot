from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import ChatType, InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery

from create_bot import bot
from main import YOUR_ADMIN_USERNAME
from states.admin_states import SendMailingState
from utils import get_subscribers


# Функция для отправки сообщения подписчикам
async def send_message_to_subscribers(message_text):
    subscribers = get_subscribers()

    for subscriber in subscribers:
        try:
            await bot.send_message(chat_id=subscriber['user_id'], text=message_text, parse_mode=types.ParseMode.HTML)
        except Exception as e:
            # Обработка ошибок при отправке сообщений
            print(f'Ошибка при отправке сообщения пользователю {subscriber.username}: {e}')


# запуск рассылок
async def start_mailing_processing(message: types.Message):
    # Проверка, что пользователь является администратором бота
    if message.chat.type == ChatType.PRIVATE and message.from_user.username == YOUR_ADMIN_USERNAME:
        # Отправка сообщения подписчикам
        await SendMailingState.start_processing.set()

        markup = InlineKeyboardMarkup(resize_keyboard=True)
        markup.add(
            InlineKeyboardButton(
                'Отмена',
                callback_data='cancel_mailing_processing:'))
        await bot.send_message(
            message.from_user.id, text="Введите текст рассылки:",
            reply_markup=markup)

    else:
        # Ответ, если пользователь не является администратором
        await bot.send_message(chat_id=message.chat.id, text='У вас нет прав для выполнения этой команды.')


async def get_mailing_text(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['mailing_text'] = message.text

        text = data['mailing_text']
    await SendMailingState.get_text.set()

    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Отмена',
            callback_data=f'cancel_mailing_processing:'))
    markup.add(
        InlineKeyboardButton(
            'Всё в порядке, запускаем',
            callback_data=f'start_mailing'))
    await message.reply(f'Проверьте, что текст в порядке:\n{text}', reply_markup=markup)


async def confirm_mailing_text(callback: CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        text = data['mailing_text']

    await send_message_to_subscribers(text)

    await callback.message.reply(f'Рассылка запущена: {text}')
    await state.finish()


def register_mailings_handlers(dp: Dispatcher):
    dp.register_message_handler(start_mailing_processing, commands='send_all', state=None)
    dp.register_message_handler(get_mailing_text, state=SendMailingState.start_processing)
    dp.register_callback_query_handler(confirm_mailing_text, state=SendMailingState.get_text)
