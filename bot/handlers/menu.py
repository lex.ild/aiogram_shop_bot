from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery, ReplyKeyboardMarkup

from create_bot import dp, bot
from utils import get_subcategories, get_products, add_to_cart, get_categories


MAIN_MENU = ["Каталог", "Корзина", "FAQ"]


async def cmd_start(message: types.Message):
    """Проверки на группу и чат закомментил, чтобы было проще тестировать"""
    # Проверка подписки на группу
    # is_member_group = await bot.get_chat_member(chat_id=GROUP_ID, user_id=message.from_user.id)

    # По аналогии роверка подписки на канал
    # is_member_channel = await bot.get_chat_member(channel_id=CHANNEL_ID, user_id=message.chat.id)

    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    for item in MAIN_MENU:
        markup.add(item)

    # Если пользователь подписан и на группу, и на канал
    # if is_member_group.status in ['member', 'creator']:
    await bot.send_message(chat_id=message.chat.id, text="Добро пожаловать!", reply_markup=markup)
    # else:
        # await bot.send_message(chat_id=message.chat.id, text="Подписка на группу и/или канал отсутствует!")


# Обработчик выбора категории
async def process_category(message: types.Message):
    # Создаем клавиатуру с подкатегориями
    keyboard = InlineKeyboardMarkup(row_width=2)
    for cat in get_categories():
        name = cat['name']
        category_id = cat['id']
        keyboard.add(InlineKeyboardButton(text=name, callback_data=f'category_id:{category_id}'))
    # Отправляем сообщение с выбором категории
    await bot.send_message(
        message.from_user.id, f'Пожалуйста, выберите категорию', reply_markup=keyboard)


# Обработчик выбора подкатегории
async def process_subcategory(callback: CallbackQuery):
    # Получаем выбранную категорию
    data = callback.data

    category_id = data.split(':')[1]

    keyboard = InlineKeyboardMarkup(row_width=2)

    for subcat in get_subcategories(category_id):
        name = subcat['name']
        subcategory_id = subcat['id']
        keyboard.add(InlineKeyboardButton(text=name, callback_data=f'subcat_id:{subcategory_id}'))

    # Отправляем сообщение с выбранной категорией и подкатегорией
    await bot.send_message(
        callback.from_user.id, f'Выберите подкатегорию', reply_markup=keyboard)
    await callback.answer()


async def process_products(callback: CallbackQuery):
    subcategory_id = callback.data.split(':')[1]
    products = get_products(subcategory_id)

    if len(products) < 1:
        await bot.send_message(callback.from_user.id, 'Тут пусто')
    else:
        # Собираем карточку товара
        for product in products:
            product_id = product['id']
            name = product['name']
            photo = product['photo']
            price = product['price']
            desc = product['description']

            markup = InlineKeyboardMarkup(resize_keyboard=True)
            markup.add(
                InlineKeyboardButton(f'Добавить в корзину - {name} {price}₽',
                                     callback_data=f'added:{product_id}:{name}'))
            text = desc

            await bot.send_photo(
                callback.from_user.id, photo=photo, caption=text, reply_markup=markup)


async def process_add_to_cart(callback: CallbackQuery):
    product_id = callback.data.split(':')[1]
    name = callback.data.split(':')[2]

    result = add_to_cart(callback.from_user.id, product_id)

    await callback.answer('Товар добавлен в корзину!')
    await bot.send_message(callback.from_user.id, f'В корзину добавлен товар: {name}\nКоличество в корзине: {result}')


def register_client_handlers(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands='start')
    dp.register_message_handler(process_category, text='Каталог')
    dp.register_callback_query_handler(process_subcategory, Text(startswith='category_id:'))
    dp.register_callback_query_handler(process_products, Text(startswith='subcat_id:'))
    dp.register_callback_query_handler(process_add_to_cart, Text(startswith='added:'))

