from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton, Message

from create_bot import bot
from states.client_states import CheckoutState
from utils import save_subscriber


async def start_checkout(callback: CallbackQuery, state: FSMContext):
    total_amount = callback.data.split(":")[1]
    await CheckoutState.start_checkout.set()
    async with state.proxy() as data:
        data['total_amount'] = total_amount
    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Отмена',
            callback_data='cancel_checkout_processing:'))
    await bot.send_message(
        callback.from_user.id, text="Давайте уточним данные для доставки.\nПожалуйста, введите имя:",
        reply_markup=markup)


async def checkout_name(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
    await CheckoutState.user_phone.set()
    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Отмена',
            callback_data=f'cancel_checkout_processing:'))
    await message.reply('Теперь введите контактный телефон', reply_markup=markup)


async def checkout_phone(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data['phone'] = message.text
    await CheckoutState.user_address.set()
    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Отмена',
            callback_data=f'cancel_checkout_processing:'))
    await message.reply(
        'Теперь введите адрес для доставки: город, улица, дом, квартира, подъезд, этаж', reply_markup=markup)


async def checkout_address(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data['address'] = message.text

        name = data['name']
        phone = data['phone']
        address = data['address']
        total_amount = data['total_amount']

    save_subscriber(message.from_user.id, name, phone, address)

    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Перейти к оплате',
            callback_data=f'transition_to_payment:{total_amount}'))
    await bot.send_message(
        message.from_user.id, text=f'Осталось только оплатить.', reply_markup=markup)
    await state.finish()


def register_checkout_handlers(dp: Dispatcher):
    dp.register_callback_query_handler(start_checkout, Text(startswith='confirm_checkout:'), state=None)
    dp.register_message_handler(checkout_name, state=CheckoutState.start_checkout)
    dp.register_message_handler(checkout_phone, state=CheckoutState.user_phone)
    dp.register_message_handler(checkout_address, state=CheckoutState.user_address)
