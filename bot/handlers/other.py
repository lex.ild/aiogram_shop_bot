from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import CallbackQuery


async def cancel_handler(callback: CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state is not None:
        # Если есть текущее состояние FSM, завершаем его
        await state.finish()
    # Отправляем ответное сообщение
    await callback.message.reply('Отменено.')


def register_other_handlers(dp: Dispatcher):
    dp.register_callback_query_handler(
        cancel_handler, lambda callback_query: callback_query.data.startswith('cancel_checkout_processing:') or
        callback_query.data.startswith('cancel_mailing_processing:'), state="*")
