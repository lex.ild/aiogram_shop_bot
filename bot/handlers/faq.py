from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.types import CallbackQuery

from create_bot import bot
from utils import get_faq_questions, search_faq, get_faq_answer


# Хэндлер для обработки команды /faq
async def process_faq_command(message: types.Message):
    # Получение списка вопросов из базы данных
    questions = get_faq_questions()

    # Формирование кнопок на основе списка вопросов
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    for question in questions:
        question_text = question[1]
        question_id = question[0]

        keyboard.add(types.InlineKeyboardButton(
            text=question_text, callback_data=f'faq_data:{question_id}'))

    # Отправка сообщения с кнопками в виде списка вопросов
    await bot.send_message(chat_id=message.chat.id, text="Выберите вопрос:", reply_markup=keyboard)


# Хэндлер для обработки нажатий на кнопки FAQ
async def process_faq_button(callback_query: CallbackQuery):
    question_id = callback_query.data.split(":")[1]
    # Получение ответа на вопрос из базы данных
    answer = get_faq_answer(question_id)
    # Отправка ответа в виде сообщения
    await bot.send_message(chat_id=callback_query.from_user.id, text=answer, parse_mode=types.ParseMode.HTML)
    await bot.answer_callback_query(callback_query.id)


# Функция-обработчик инлайн-запросов
async def inline_faq_query(query: types.InlineQuery):
    # Получение текста запроса
    query_text = query.query
    results = []
    # Поиск вопросов и ответов в бд
    faq_results = search_faq(query_text)
    for result in faq_results:
        # Создание инлайн-ответов
        answer = types.InlineQueryResultArticle(
            id=result['id'],
            title=result['question'],
            input_message_content=types.InputTextMessageContent(
                message_text=result['answer'],
                parse_mode=types.ParseMode.HTML
            )
        )
        results.append(answer)
    await bot.answer_inline_query(query.id, results)


def register_faq_handlers(dp: Dispatcher):
    dp.register_message_handler(process_faq_command, text='FAQ')
    dp.register_callback_query_handler(process_faq_button, (Text(startswith='faq_data:')))
    dp.register_inline_handler(inline_faq_query)
