from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from create_bot import bot
from utils import get_cart_by_user, change_cart_quantity


async def process_cart(message: types.Message):

    user_cart = get_cart_by_user(message.from_user.id)

    if len(user_cart) < 1:
        await message.answer('Ваша корзина пуста.')
    else:
        for product in user_cart:
            name = product['name']
            photo = product['photo']
            price = product['price']
            quantity = product['quantity']
            desc = product['description']
            cart_id = product['cart_id']

            markup = InlineKeyboardMarkup(resize_keyboard=True)
            markup.add(
                InlineKeyboardButton(
                    f'Удалить - {name} {price}₽ Количество: {quantity}',
                    callback_data=f'del:{message.from_user.id}:{cart_id}:{name}:{price}'))
            markup.add(
                InlineKeyboardButton(
                    '➕',
                    callback_data=f'increase:{message.from_user.id}:{cart_id}:{name}:{price}'))
            markup.add(
                InlineKeyboardButton(
                    '➖',
                    callback_data=f'decrease:{message.from_user.id}:{cart_id}:{name}:{price}'))

            await bot.send_photo(
                message.from_user.id, photo=photo, caption=desc, reply_markup=markup)

        markup = InlineKeyboardMarkup(resize_keyboard=True)
        markup.add(
            InlineKeyboardButton(
                'Оформляем',
                callback_data=f'checkout:{message.from_user.id}'))
        await bot.send_message(message.from_user.id, text='Оформить заказ?', reply_markup=markup)


async def change_quantity(callback: types.CallbackQuery):
    action_type = callback.data.split(':')[0]
    cart_id = callback.data.split(':')[2]
    name = callback.data.split(':')[3]
    price = callback.data.split(':')[4]

    quantity = change_cart_quantity(cart_id, action_type)

    if quantity in (0, None):
        await callback.answer('Товар удалён из корзины')
        await callback.message.delete()
    else:
        markup = InlineKeyboardMarkup()
        markup.add(
            InlineKeyboardButton(
                f'Удалить - {name} {price}₽ Количество: {quantity} ',
                callback_data=f'del:{callback.from_user.id}:{cart_id}:{name}:{price}'))
        markup.add(
            InlineKeyboardButton(
                '➕', callback_data=f'increase:{callback.from_user.id}:{cart_id}:{name}:{price}'))
        markup.add(
            InlineKeyboardButton(
                '➖', callback_data=f'decrease:{callback.from_user.id}:{cart_id}:{name}:{price}'))
        await callback.message.edit_reply_markup(markup)


async def confirm_checkout(message: types.Message):
    cart_products = get_cart_by_user(message.from_user.id)
    text = 'Давайте проверим содержимое корзины:\n'
    total_amount = 0
    for product in cart_products:
        product_amount = product['quantity'] * product['price']

        text += f'{product["name"]} - {product["quantity"]} - {product_amount}\n'

        total_amount += product_amount

    text += f'Общая сумма к оплате: {total_amount}'
    await bot.send_message(message.from_user.id, text)
    markup = InlineKeyboardMarkup(resize_keyboard=True)
    markup.add(
        InlineKeyboardButton(
            'Все верно, продолжить',
            callback_data=f'confirm_checkout:{total_amount}'))
    markup.add(
        InlineKeyboardButton(
            'Вернуться в корзину',
            callback_data=f'back_to_cart:'))

    await bot.send_message(message.from_user.id, text='Всё указано верно?', reply_markup=markup)


def register_cart_handlers(dp: Dispatcher):
    dp.register_message_handler(process_cart, text='Корзина')
    dp.register_callback_query_handler(process_cart, Text(startswith='back_to_cart:'))
    dp.register_callback_query_handler(
        change_quantity, lambda callback_query: callback_query.data.startswith('increase:')
        or callback_query.data.startswith('decrease:') or callback_query.data.startswith('del:'))
    dp.register_callback_query_handler(confirm_checkout, Text(startswith='checkout:'))
