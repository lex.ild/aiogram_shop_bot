import os
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.types import CallbackQuery, ContentType, PreCheckoutQuery

from create_bot import dp, bot
from utils import delete_all_cart_items, save_order_to_excel


async def buy(callback: CallbackQuery):
    total_cart_amount = callback.data.split(":")[1]
    amount_payable = int(float(total_cart_amount)) * 100
    price = types.LabeledPrice(label="товар", amount=amount_payable)
    await bot.send_invoice(callback.from_user.id,
                           title="Оплата заказа в боте",
                           description="товары из корзины",
                           provider_token=os.environ.get("PAYMENT_TOKEN"),
                           currency="rub",
                           photo_width=416,
                           photo_height=234,
                           photo_size=416,
                           is_flexible=False,
                           prices=[price],
                           payload="test-payload")


# pre checkout  (must be answered in 10 seconds)
async def pre_checkout_query(pre_checkout_q: types.PreCheckoutQuery):
    await bot.answer_pre_checkout_query(pre_checkout_q.id, ok=True)


# successful payment
async def successful_payment(message: types.Message):
    print("SUCCESSFUL PAYMENT:")
    payment_info = message.successful_payment.to_python()
    for k, v in payment_info.items():
        print(f"{k} = {v}")
    # очищаем корзину при успешной оплате
    delete_all_cart_items(message.from_user.id)

    total_amount = message.successful_payment.total_amount // 100

    save_order_to_excel(message.from_user.id, total_amount)

    await bot.send_message(message.chat.id,
                           f"Платеж на сумму {total_amount} {message.successful_payment.currency} прошел успешно.\n"
                           f"Ожидайте, с вами свяжется наш сотрудник.")


def register_payment_handlers(dp: Dispatcher):
    dp.register_pre_checkout_query_handler(pre_checkout_query, lambda query: True)
    dp.register_callback_query_handler(buy, Text(startswith='transition_to_payment'))
    dp.register_message_handler(successful_payment, content_types=ContentType.SUCCESSFUL_PAYMENT)

