from django.contrib import admin

from .models import Category, Subcategory, Product, Cart, Subscriber, Faq


admin.site.register(Subscriber)
admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(Faq)

