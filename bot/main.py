import logging

from handlers import menu, cart, checkout, payment, other, faq, mailings
from aiogram.utils import executor
from create_bot import dp, storage


GROUP_ID = '-1001939975555'  # ID вашей группы, по членству в которой идёт проверка
CHANNEL_ID = 'id'  # ID вашего канала, по которому идёт проверка
YOUR_ADMIN_USERNAME = 'undefined_ind'


async def on_startup(_):
    logging.basicConfig(level=logging.INFO)

    menu.register_client_handlers(dp)
    cart.register_cart_handlers(dp)
    checkout.register_checkout_handlers(dp)
    payment.register_payment_handlers(dp)
    other.register_other_handlers(dp)
    faq.register_faq_handlers(dp)
    mailings.register_mailings_handlers(dp)


async def shutdown(_):
    await storage.close()

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup, on_shutdown=shutdown)

